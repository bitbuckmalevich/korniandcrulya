<?php 
  // Important: Global - Disable error 
  // error_reporting(0);
  
  // Important: Global - Including router module 
  include($_SERVER['DOCUMENT_ROOT'].'/router.php'); 
  
  // Important: Global - Including routed view
  include($_SERVER['DOCUMENT_ROOT'].$page_path); 
?>